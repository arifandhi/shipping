package com.marketplace.shipping.controller;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ControllerShippingApi {
    
    private Random random = new Random();

    // http://app/api/cost/jkt/bgr/12
    @GetMapping("/api/cost/{asal}/{tujuan}/{berat}")
    public Map<String, String> hitungBiaya(@PathVariable String asal, @PathVariable String tujuan, @PathVariable BigDecimal berat){
        Map<String, String> hasil = new LinkedHashMap<>();
        hasil.put("asal", asal);
        hasil.put("tujuan", tujuan);
        hasil.put("berat", berat.toString());
        hasil.put("reguler", String.valueOf(random.nextInt(100000)+10000));
        hasil.put("kilat", String.valueOf(random.nextInt(200000)+20000));
        return hasil;
    }

}